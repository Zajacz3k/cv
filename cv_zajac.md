# Bartłomiej Zając

### Kontakt
Adres: ul. Wyzwolenia 69, Lublin 20-368
Email: [brtkzajc@gmail.com](mailto:brtkzajc@gmai;.com)  

### Edukacja

* 2012-2015 - _Liceum Ogólnokształcące nr 14_
* 2015-2019 - **UMCS**

### Doświadczenie

* 2015 - Sinah Warren Hotel
* 2017 - GIS-Expert
* 2019 - Hersheypark

### Umiejętności
* obsługa komputera
* język angielski